package com.wishkart.sales.services;

import com.wishkart.sales.exceptionHandling.OrderUnsuccessfullException;
import com.wishkart.sales.models.Kart;
import com.wishkart.sales.models.KartItem;

public interface SalesService {
	
	Kart addToKart(KartItem kartItem, String userId);
	Kart addAddress(Kart kart);
	Kart addPayment(Kart kart);
	Integer placeOrder(Kart kart);
	String checkOut(Kart kart) throws OrderUnsuccessfullException;
}
