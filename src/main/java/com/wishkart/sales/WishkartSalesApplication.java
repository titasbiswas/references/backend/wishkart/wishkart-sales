package com.wishkart.sales;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient
@SpringBootApplication
public class WishkartSalesApplication {

	public static void main(String[] args) {
		SpringApplication.run(WishkartSalesApplication.class, args);
	}
}
