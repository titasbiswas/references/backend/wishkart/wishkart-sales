package com.wishkart.sales.exceptionHandling;

public class OrderUnsuccessfullException extends Exception {
	public OrderUnsuccessfullException(String message,  Throwable th) {
		super(message, th);
	}
}
