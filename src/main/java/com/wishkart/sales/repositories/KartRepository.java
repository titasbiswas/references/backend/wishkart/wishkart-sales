package com.wishkart.sales.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.wishkart.sales.models.Kart;

@Repository
public interface KartRepository extends JpaRepository<Kart, Integer> {

	Optional<Kart> findByUserId(String userId);
}
