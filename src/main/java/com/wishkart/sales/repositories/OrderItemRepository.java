package com.wishkart.sales.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.wishkart.sales.models.OrderItem;

public interface OrderItemRepository extends JpaRepository<OrderItem, Long> {

}
