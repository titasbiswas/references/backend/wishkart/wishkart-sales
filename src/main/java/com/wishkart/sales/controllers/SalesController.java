package com.wishkart.sales.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.wishkart.sales.exceptionHandling.OrderUnsuccessfullException;
import com.wishkart.sales.models.Kart;
import com.wishkart.sales.models.OrderItem;
import com.wishkart.sales.repositories.KartRepository;
import com.wishkart.sales.repositories.OrderItemRepository;
import com.wishkart.sales.services.SalesService;

@RestController
@RequestMapping("/sales")
public class SalesController {

	@Autowired
	private KartRepository kartRepo;
	
	@Autowired
	private OrderItemRepository orderItemRepo;

	@Autowired
	private SalesService salesService;

	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<List<Kart>> getAllKarts() {
		return ResponseEntity.ok(kartRepo.findAll());
	}

	/**
	 * NOT TO BE USED IN GENERAL PURPOSE
	 * It expects a fully populated kart object with items in it
	 * @param kart
	 * @return
	 */
	@PostMapping(produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<Kart> addFullKart(@RequestBody Kart kart) {
		return ResponseEntity.ok(kartRepo.save(kart));
	}

	/**
	 * @param kart
	 * @return ResponseEntity<Kart> 
	 * @see Client would sent a kart object with only one
	 *         kartItem in it which requires to be added to the kart of that user
	 */
	@PostMapping(value = "/addtocart", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<Kart> addToKart(@RequestBody Kart kart) {
		return ResponseEntity.ok(salesService.addToKart(kart.getKartItems().get(0), kart.getUserId()));
	}
	
	
	
	/**
	 * @param kart
	 * @return Order id in string format
	 * @throws OrderUnsuccessfullException 
	 * @see Client will call this method after successfull payment
	 */
	@PostMapping(value = "/checkout", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<HashMap<String, String>> checkOut(@RequestBody Kart kart) throws OrderUnsuccessfullException {
		return ResponseEntity.ok(new HashMap<String, String>().put("orderId", salesService.checkOut(kart)));
	}
	
	@PutMapping(value="/updatedeliverystat", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	void updateDeliveryStatus(@RequestBody final OrderItem item) {
		orderItemRepo.findById(item.getId()).ifPresent(i -> {
			i.setDeliveredOn(item.getDeliveredOn());
			i.setDeliveryComment(item.getDeliveryComment());
			orderItemRepo.save(i);
		});
	}
}
