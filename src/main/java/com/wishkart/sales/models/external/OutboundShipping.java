package com.wishkart.sales.models.external;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.ToString;

import java.sql.Timestamp;

/**
 * The persistent class for the outbound_shipping database table.
 * 
 */

@Data
@Builder
@AllArgsConstructor
@ToString
public class OutboundShipping implements Serializable {
	private static final long serialVersionUID = 1L;

	private Long id;

	private Long billAddrId;

	private Timestamp deliveredOn;

	private String deliveryComment;

	private String deliveryStatus;

	private Long itemId;

	private Long orderId;

	private Integer qty;

	private Long shipAddrId;

	private Timestamp shippedOn;

	private Integer trackingId;
	
	private String productId;

	public OutboundShipping() {
	}
}