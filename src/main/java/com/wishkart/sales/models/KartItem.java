package com.wishkart.sales.models;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

import java.math.BigDecimal;


/**
 * The persistent class for the kart_items database table.
 * 
 */
@Entity
@Data
@Table(name="kart_items")
@NamedQuery(name="KartItem.findAll", query="SELECT k FROM KartItem k")
public class KartItem implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name="discount_prcntg")
	private BigDecimal discountPrcntg;

	@Column(name="line_total")
	private BigDecimal lineTotal;

	@Column(name="no_of_product")
	private Integer noOfProduct;

	@Column(name="product_id")
	private String productId;

	private BigDecimal rate;

	//bi-directional many-to-one association to Kart
	@JsonIgnore
	@ManyToOne
	private Kart kart;

	public KartItem() {
	}


}