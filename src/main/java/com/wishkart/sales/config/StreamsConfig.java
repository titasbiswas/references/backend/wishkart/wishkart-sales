package com.wishkart.sales.config;

import org.springframework.cloud.stream.annotation.EnableBinding;

import com.wishkart.sales.messaging.ShippingStreams;

@EnableBinding(ShippingStreams.class)
public class StreamsConfig {

}
